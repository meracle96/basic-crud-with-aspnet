﻿using System;
using Microsoft.EntityFrameworkCore;
using TestYoutube.Data;

namespace TestYoutube.Models
{
    public class StudentContext : ApplicationDbContext
    {
        public StudentContext(DbContextOptions<ApplicationDbContext> options) : base(options)
        {
        }

        public DbSet<Student> Students { get; set; }
    }

    public class Student
	{
		public int id { get; set; }
		public string name { get; set; }
		public string email { get; set; }
	}
}

