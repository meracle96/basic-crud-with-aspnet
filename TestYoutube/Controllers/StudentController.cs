﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using TestYoutube.Data;
using TestYoutube.Models;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace TestYoutube.Controllers
{
    public class StudentController : Controller
    {
        private readonly ApplicationDbContext _context;

        public StudentController(ApplicationDbContext context) {
            _context = context;
	    }

        // GET: /<controller>/
        public IActionResult Index()
        {
            var students = _context.Students.ToList();
            return View(students);
        }

        public IActionResult Create()
        {
            return View();
	    }

        [HttpPost]
        public IActionResult Create(Student student)
        {
            _context.Students.Add(student);
            _context.SaveChanges();
            return View();
	    }

        public IActionResult Edit(int id)
        {
            var item = _context.Students.Find(id);
            if(item == null) {
                return NotFound();
            }
            return View(item);
	    }

        [HttpPost]
        public IActionResult Edit(int id, Student student)
        {
            if(id != student.id) {
                return NotFound();
	        }
            if(ModelState.IsValid) {
                _context.Update(student);
                _context.SaveChanges();
	        }
            return View(student);
	    }

        public IActionResult Delete(int id)
        {
            var item = _context.Students.Find(id);
            if (item == null)
            {
                return NotFound();
            }

            return View(item);
        }

        [HttpPost]
        public IActionResult Delete(int id, Student student)
        {
            if(id != student.id) {
                return NotFound();
	        }
            if(ModelState.IsValid) {
                _context.Students.Remove(student);
                _context.SaveChanges();
	        }
            return RedirectToAction(nameof(Index));
	    }
    }
}

